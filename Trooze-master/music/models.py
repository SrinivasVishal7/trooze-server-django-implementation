import os
from django.db import models
from django.contrib.auth.models import User
from django.forms.models import model_to_dict
from django.dispatch import receiver
from django.core.cache import cache
from django.db.models import Q
import music.model_funcs as fn
from .const import (
    MUSICAL_GENRES,
    COUNTRIES,
    AREAS,
    GENDER,
    MONEY,
)

"""""""""""
  Artist
"""""""""""


class Artist(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    stage_name = models.CharField(max_length=50, blank=True)
    gender = models.CharField(max_length=10, choices=GENDER, default=GENDER[0][0])
    country = models.CharField(max_length=50, choices=COUNTRIES, default=COUNTRIES[0][0])
    area = models.CharField(max_length=10, choices=AREAS, default=AREAS[0][0])
    picture = models.FileField()
    likers = models.ManyToManyField(User, related_name='artist_field', blank=True)
    followers = models.ManyToManyField(User, related_name='artists', blank=True)
    about = models.TextField(blank=True)
    facebook = models.URLField(blank=True)
    instagram = models.URLField(blank=True)
    twitter = models.URLField(blank=True)
    created = models.DateTimeField(blank=True)
    updated = models.DateTimeField(blank=True)

    def save(self, *args, **kwargs):
        fn.save(self, *args, **kwargs)
        self.cache_artist()

    def as_object(self):  # turn django model into a plain object
        return fn.as_object(self)

    @property
    def full_name(self):
        return self.first_name + ' ' + self.last_name

    @property
    def location(self):
        return self.country + ', ' + self.area

    def update_fields(self, updater, fields=None, exclude=None):  # update model fields
        fn.update_fields(self, updater, fields, exclude)

    def is_followed_by(self, user):
        return self.followers.all().filter(id=user.id).exists()

    def is_liked_by(self, user):
        return self.likers.all().filter(id=user.id).exists()

    def cache_artist(self):
        cache.set(self.__cache_name__, self)

    @property
    def __cache_name__(self):
        return 'artists.{0}'.format(self.pk)

    @classmethod
    def cached_artist_by_pk(cls, pk, related=None):
        return fn.cached_object_by_pk(cls, pk, related=related)

    def __str__(self):
        return 'Artist username- {0}'.format(self.user.username)


# save artist when user saved
@receiver(models.signals.post_save, sender=User)
def save_artist_on_save_user(sender, instance, created, **kwargs):
    if created and hasattr(instance, 'artist'):
        artist = instance.artist
        artist.user = instance
        artist.save()


"""""""""""""""""""""
    Recommendation
"""""""""""""""""""""


class Recommendation(models.Model):
    on_user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField()
    date = models.DateTimeField()
    rate = models.PositiveIntegerField(default=1)
    created = models.DateTimeField(blank=True)
    updated = models.DateTimeField(blank=True)

    def save(self, *args, **kwargs):
        fn.save(self, *args, **kwargs)

    class Meta:
        ordering = ('-date', '-rate',)


"""""""""
  Event
"""""""""


class Event(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    start_time = models.CharField(max_length=5)
    end_time = models.CharField(max_length=5)
    price = models.FloatField(default=0.0)
    currency = models.CharField(max_length=60, choices=MONEY, default=MONEY[0][0])
    location = models.CharField(max_length=90)
    country = models.CharField(max_length=70)
    city = models.CharField(max_length=70)
    street = models.CharField(max_length=80)
    street_number = models.CharField(max_length=6)
    place_image = models.FileField()
    description = models.TextField()
    people_limit = models.PositiveIntegerField(blank=True, null=True)
    participants = models.ManyToManyField(User, related_name='events')
    subscribers = models.ManyToManyField(User, related_name='events_list')
    likers = models.ManyToManyField(User, related_name='events_arr')
    created = models.DateTimeField(blank=True)
    updated = models.DateTimeField(blank=True)

    class Meta:
        ordering = ('start_date',)

    def save(self, *args, **kwargs):
        fn.save(self, *args, **kwargs)
        self.cache_event()

    def as_object(self):
        return fn.as_object(self)

    @property
    def is_free(self):
        return int(self.price) == 0

    @property
    def num_people(self):
        return self.participants.count()

    @property
    def num_subscribers(self):
        return self.subscribers.count()

    @property
    def likes(self):
        return self.likers.count()

    @property
    def get_location(self):
        return self.country + ', ' + self.city

    @property
    def get_address(self):
        return self.street + ' ' + self.street_number

    @property
    def get_currency(self):
        symbol = self.currency.split('(')[1].replace(')', '')
        if symbol:
            return symbol
        else:
            return self.currency.split('(')[0]

    @property
    def days_duration(self):
        return self.end_date - self.start_date

    @property
    def has_room_left(self):
        if not self.people_limit:
            return True
        return self.num_people < self.people_limit

    def update_fields(self, updater, fields=None, exclude=None):  # update model fields
        fn.update_fields(self, updater, fields, exclude)

    def is_user_joined(self, user):
        return self.participants.all().filter(id=user.id).exists()

    def is_user_subscribed(self, user):
        return self.subscribers.all().filter(id=user.id).exists()

    def is_liked_by(self, user):
        return self.likers.all().filter(id=user.id).exists()

    @property
    def __cache_name__(self):
        return 'events.{0}'.format(self.pk)

    def cache_event(self):
        cache.set(self.__cache_name__, self)

    @classmethod
    def cached_event_by_pk(cls, pk, related=None):
        return fn.cached_object_by_pk(cls, pk, related=related)


# make sure to remove event image file when model deleted
@receiver(models.signals.post_delete, sender=Event)
def auto_delete_event_image_on_delete(sender, instance, **kwargs):
    if instance.place_image:
        if os.path.isfile(instance.place_image.path):  # check if file exists
            os.remove(instance.place_image.path)  # remove from storage
        cache.delete(instance.__cache_name__)  # delete cache


"""""""""""""""
 Notification
"""""""""""""""


class Notification(models.Model):
    by = models.ForeignKey(User)
    message = models.TextField()
    date = models.DateTimeField()
    is_critical = models.BooleanField(default=False)

    class Meta:
        ordering = ('-is_critical', '-date',)


"""""""""
  Album
"""""""""


class Album(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    artist = models.CharField(max_length=35)
    name = models.CharField(max_length=60)
    genre = models.CharField(max_length=50, choices=MUSICAL_GENRES)
    logo = models.URLField(blank=True)
    logo_file = models.FileField(blank=True, null=True)
    likers = models.ManyToManyField(User, related_name="albums")
    created = models.DateTimeField(blank=True)
    updated = models.DateTimeField(blank=True)

    class Meta:
        ordering = ('name',)

    def save(self, *args, **kwargs):  # could also be done using pre_save signal
        fn.save(self, *args, **kwargs)
        self.cache_album()

    def as_object(self):  # turn django model into a plain object
        return fn.as_object(self)

    def update_fields(self, updater, fields=None, exclude=None):  # update model fields
        fn.update_fields(self, updater, fields, exclude)

    def get_copy(self):
        return model_to_dict(self, exclude=['id', 'created', 'updated', 'user', 'likers'])

    def is_liked_by(self, user):
        return self.likers.all().filter(id=user.id).exists()

    @property
    def has_downloadable_songs(self):
        return self.song_set.filter(
            Q(is_downloadable=True) &
            Q(is_playable=True)
        ).exists()

    @classmethod
    def get_albums_by_user(cls, user):
        return cls.objects.filter(user=user)

    def cache_album(self):
        cache.set(self.__cache_name__, self)

    @property
    def __cache_name__(self):
        return 'albums.{0}'.format(self.pk)

    @classmethod
    def cached_album_by_pk(cls, pk, related=None):
        return fn.cached_object_by_pk(cls, pk, related=related)

    def __str__(self):
        return 'Album: {0}, Artist: {1}'.format(self.name, self.artist)


# make sure to remove image file when model deleted
@receiver(models.signals.post_delete, sender=Album)
def auto_delete_image_on_delete(sender, instance, **kwargs):
    if instance.logo_file:
        if os.path.isfile(instance.logo_file.path):  # check if file exists
            os.remove(instance.logo_file.path)  # remove from storage
    cache.delete(instance.__cache_name__)  # delete from cache


"""""""""
  Song
"""""""""


class Song(models.Model):
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    audio_file = models.FileField()
    is_favorite = models.BooleanField(default=False)
    is_downloadable = models.BooleanField(default=True)
    is_playable = models.BooleanField(default=True)
    created = models.DateTimeField(blank=True)
    updated = models.DateTimeField(blank=True)

    class Meta:
        ordering = ('-is_favorite',)

    def save(self, *args, **kwargs):  # could also be done using pre_save signal
        fn.save(self, *args, **kwargs)
        self.cache_song()

    def as_object(self):  # turn django model into a plain object
        return fn.as_object(self)

    def update_fields(self, updater, fields=None, exclude=None):  # update model fields
        fn.update_fields(self, updater, fields, exclude)

    def get_copy(self):
        return model_to_dict(self, exclude=['id', 'created', 'updated', 'album', 'is_favorite'])

    def cache_song(self):
        cache.set(self.__cache_name__, self)

    @property
    def __cache_name__(self):
        return 'songs.{0}'.format(self.pk)

    @classmethod
    def cached_song_by_pk(cls, pk, related=None):
        return fn.cached_object_by_pk(cls, pk, related=related)

    def __str__(self):
        return 'Song: {0}, album: {1}'.format(self.name, self.album.name)


# make sure to remove audio file when model deleted
@receiver(models.signals.post_delete, sender=Song)
def auto_delete_audio_on_delete(sender, instance, **kwargs):
    if os.path.isfile(instance.audio_file.path):  # check if file exists
        os.remove(instance.audio_file.path)  # remove from storage
    cache.delete(instance.__cache_name__)  # delete cache
