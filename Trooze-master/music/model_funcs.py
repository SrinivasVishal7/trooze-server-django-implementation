import os
from django.forms.models import model_to_dict
from django.core.cache import cache
from django.utils import timezone
from django.shortcuts import get_object_or_404


def save(self, *args, **kwargs):  # could also be done using pre_save signal
    if not self.id:
        self.created = timezone.now()
    self.updated = timezone.now()
    if hasattr(self, 'file_to_delete'):
        if os.path.isfile(self.file_to_delete.path):  # check if file exists
            os.remove(self.file_to_delete.path)  # remove from storage
            del self.file_to_delete  # delete extra attribute
    super(self.__class__, self).save(*args, **kwargs)


def as_object(self):  # turn django model into a plain object
    return model_to_dict(self)


def update_fields(self, updater, fields=None, exclude=None):  # update model fields
    fields = fields or self.as_object().keys()
    if exclude:
        fields -= (['id', 'created', 'updated'] + exclude)
    updater = updater.as_object()
    for field in fields:
        setattr(self, field, updater[field])


def cached_object_by_pk(cls, pk, name='', related=None):
    name = name or (cls.__name__.lower() + 's')
    key_name = '{0}.{1}'.format(name, pk)
    obj = cache.get(key_name)
    if obj is None:
        if related:
            obj = cls.objects.select_related(related).get(pk=pk)
        else:
            obj = get_object_or_404(cls, pk=pk)
        cache.set(key_name, obj)
    return obj
