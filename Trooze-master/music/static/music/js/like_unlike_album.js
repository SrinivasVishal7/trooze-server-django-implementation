$(document).ready(function() {

$('.album-like-unlike').click(function(e) {
    e.preventDefault();
    var $link = $(this);
    var URL = $link.attr('href');
    $.ajax({
        method: 'get',
        url: URL,
        dataType: 'json',
        contentType: 'application/json',
        success: function(result, status, xhr) {
            var $label = $link.parent().prev().find('.like-badge');
            var $glyph = $link.find('.glyphicon');
            var $text = $link.find('.like-unlike');
            var likes = result && result.likes;
            var label_text = likes + (likes > 1 ? ' likes': ' like');
            $label.text(label_text);
            // check if action was unliking
            if($glyph.hasClass('glyphicon-thumbs-down')) {
                $link.attr('href', URL.replace('unlike', 'like'));
                $glyph.removeClass('glyphicon-thumbs-down').addClass('glyphicon-thumbs-up');
                $text.text('Like');
            }
            // action was liking
            else {
                $link.attr('href', URL.replace('like', 'unlike'));
                $glyph.removeClass('glyphicon-thumbs-up').addClass('glyphicon-thumbs-down');
                $text.text('Unlike');
            }
        },
        error: function(result, status, xhr) {
            alert('Some error has occurred');
        }
    });
});

});