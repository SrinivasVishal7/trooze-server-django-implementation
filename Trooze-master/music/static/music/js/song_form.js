$(document).ready(function() {

    var $isDownloadable = $('#id_is_downloadable').checkbox();
    var $isPlayable = $('#id_is_playable').checkbox();

    // if not playable, not downloadable
    downloadController();

    // if playable, let choose downloadable
    $isPlayable.click(downloadController);

    // on label click
    $isPlayable.prev().prev().click(function() {
        if($isPlayable.prev().is(':checked')) {
            return disableDownload();
        }
        enableDownload();
    });

    function downloadController() {
        if(!$isPlayable.prev().is(':checked')) {
            return disableDownload();
        }
        enableDownload();
    }

    function disableDownload() {
        $isDownloadable.parent().css('display', 'none');
    }

    function enableDownload() {
        $isDownloadable.parent().css('display', '');
    }

});