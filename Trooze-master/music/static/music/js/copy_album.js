$(document).ready(function() {

    // copy album
    $('.copy-album').click(function(e) {
        e.preventDefault();
        var $link = $(this);
        var COPY_URL = $link.attr('href');

        var $overlay = $('#overlay');
        var $copy_popup = $('#confirm-copy-popup');
        $copy_popup.find('#album-name').text($link.data('name')); // fill name
        $copy_popup.css('display', 'block');
        $overlay.css('display', 'block');

        var $form = $copy_popup.find('form');
        $form.attr('action', COPY_URL); // set url as form action attribute
        $('.cancel-copy, #overlay').on('click', function() {
            $copy_popup.css('display', 'none');
            $overlay.css('display', 'none');
            $('.cancel-copy, #overlay').off('click');
            return false;
        });

    });

});