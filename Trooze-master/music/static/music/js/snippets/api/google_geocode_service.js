(function() {

function initMap() {
    // init geocoder object
    var geocoder = new google.maps.Geocoder();
    // make request to geocoder service passing the address
    geocoder.geocode({'address': absoluteAddress}, function(results, status) {
        if(status == 'OK') {
            var result = results[0];
            if(!(result && result.geometry && result.geometry.location)) {
                return;
            }
            var mapCenter = result.geometry.location;
            // render map
            var mapNode = document.getElementById('map');
            var map = new google.maps.Map(_getStyledMap(mapNode), {
                zoom: 15,
                center: mapCenter
            });
            // set marker on map
            var marker = new google.maps.Marker({
                map: map,
                position: mapCenter
            });
        }
    });
}

function _getStyledMap(mapNode) {
    mapNode.style.height = '250px';
    mapNode.style.boxShadow = '0 0 0 2px #e4e3e3';
    mapNode.style.margin = '40px 5px 10px';
    mapNode.style.borderRadius = '3px';
    return mapNode;
}

// expose initMap callback as global
window.initMap = initMap;
})();