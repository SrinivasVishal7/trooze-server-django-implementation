(function() {

// dom nodes
var $location = document.getElementById('id_location');
var $country = document.getElementById('id_country');
var $city = document.getElementById('id_city');
var $street = document.getElementById('id_street');
var $streetNumber = document.getElementById('id_street_number');

// auto complete object
var autoComplete;

// auto complete callback
function initAutocomplete() {
    var location_input = document.getElementById('id_location');
    // configure the auto complete address type on input element
    autoComplete = new google.maps.places.Autocomplete(location_input, {
        types: ['address']
    });
    // set listener to user key press changing places
    autoComplete.addListener('place_changed', getAddressOptions);
}

function getAddressOptions() {
    // place from auto complete object
    var place = autoComplete.getPlace();
    for (var i=0; i < place.address_components.length; i++) {
      var addressComponent = place.address_components[i];
      var addressType = addressComponent.types[0];
      if(addressType == 'street_number') {
        $streetNumber.value = addressComponent.long_name;
      }
      else if(addressType == 'route') {
        $street.value = addressComponent.long_name;
      }
      else if(addressType == 'locality') {
        $city.value = addressComponent.long_name;
      }
      else if(addressType == 'country') {
        $country.value = addressComponent.long_name;
      }
    }
}

// set focus event on auto complete input element
// to try search by current location radius
$location.addEventListener('focus', geoLocate);

// input focus listener callback function
function geoLocate() {
    // check if browser supports this feature
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            // geolocation object
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            }
            // circle object for setting coordinates and radius
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            // apply on auto complete object
            autoComplete.setBounds(circle.getBounds());
        });
    }
}

// expose function as global
window.initAutocomplete = initAutocomplete;

})();