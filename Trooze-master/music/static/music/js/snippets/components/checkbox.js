jQuery.fn.extend({

    checkbox: function($input) {
        // hide input
        $input = this.css('display', 'none');

        // assign vars & dom elements
        var shouldCheckNext = $input.is(':checked');
        var $label = $input.prev();
        var $virtualCheckbox = $('<i class="virtual-checkbox fa">');

        // append virtual checkbox
        $label.css('cursor', 'pointer')
              .parent().append($virtualCheckbox);

        // checkbox controller
        var controller = {
            // take care of virtual checkbox view
            checkView: function() {
                shouldCheckNext ?
                    $virtualCheckbox.addClass('fa-check') :
                    $virtualCheckbox.removeClass('fa-check');
                shouldCheckNext = !shouldCheckNext;
            },
            // init controller functionality
            init: function() {
                var _this = this;
                // click checkbox input when virtual checkbox clicked
                $virtualCheckbox.click(function() {
                    _this.checkView();
                    $input.click();
                });
                // make check view respond to label click
                $label.click(_this.checkView);
            }
        }

        // init controller
        controller.checkView();
        controller.init();

        // return virtual checkbox
        return $virtualCheckbox;

    }

});