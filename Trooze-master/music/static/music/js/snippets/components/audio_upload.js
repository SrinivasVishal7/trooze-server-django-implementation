jQuery.fn.extend({

    uploadAudio: function(URL) {

        // globals
        var $filePopup, $$dragZone, $audio, $err, file;
        var xhr = new XMLHttpRequest();
        var audioRegex = /^audio\/(mp3|mpeg|wav|ogg)$/i;

        var template_string = `
            <div class="confirm-popup file-upload-popup panel panel-default text-center">
                <div>
                    <div class="panel-heading">
                        <h3>Upload Song</h3>
                    </div>
                    <div class="panel-body">
                        <div class="file-drag">
                            <p>Drag & Drop or Choose from folder</p>
                        </div>
                        <div class="progress photo-upload-progress">
                          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                            Uploading...
                          </div>
                        </div>
                        <div class="text-danger error-message"></div>
                    </div>
                    <input type="file" id="file" accept="audio/*" />
                    <div class="buttons">
                        <button class="btn btn-danger" id="upload" type="submit">Upload</button>
                        <button class="btn btn-default cancel">Cancel</button>
                    </div>
                </div>
            </div>
        `;

        // attach click listener on upload element
        this.on('click', function(e) {
            e.preventDefault();

            // get dom elements
            var $overlay = $('#overlay');
            $filePopup = $(template_string);
            $$dragZone = $filePopup.find('.file-drag').get(0);
            var $progressbar = $filePopup.find('.photo-upload-progress > .progress-bar').width(0);
            $err = $filePopup.find('.error-message');
            var $$input = $filePopup.find('#file').get(0);

            // when drag zone clicked, let choose from folder
            $$dragZone.addEventListener('click', function(e) { $$input.click(); });

            // set listeners to show file
            $$input.addEventListener('change', fileSelectHandler);
            $$dragZone.addEventListener('drop', fileSelectHandler);
            ['dragover', 'dragenter', 'dragleave'].forEach(function(eventType) {
                $$dragZone.addEventListener(eventType, dragHandler);
            });

            // display upload form on the page
            $(document.body).append($filePopup);
            $filePopup.css('display', 'block');
            $overlay.css('display', 'block');

            // kill upload photo popup
            $('.cancel, #overlay').on('click', function() {
                xhr.abort(); // abort upload
                $filePopup.remove();
                $overlay.css('display', 'none');
                $('#overlay').off('click');
                if($audio) { // if audio, make sure it stops playing
                    $audio.get(0).pause();
                }
                return false;
            });

            // upload image to server
            $('#upload').click(function(e) {
                e.preventDefault();
                if(!file || !file.type) {
                    return $err.text('A file must be supplied');
                }
                if(!audioRegex.test(file.type)) { // test file type
                    return $err.text('File type must be mp3, wav or ogg');
                }
                // show progress bar
                $progressbar.css('display', 'block');
                // xhr on progress
                xhr.upload.onprogress = function(e) {
                    if (e.lengthComputable) {
                        var upload_percentage = Math.round((e.loaded * 100) / e.total);
                        $progressbar.width(upload_percentage + '%'); // show upload progress
                    }
                }
                // xhr on load
                xhr.upload.onload = function(e) {
                    $progressbar.css('display', 'none').width(0);
                }
                xhr.onreadystatechange = function() {
                    if(xhr.readyState == 4 && xhr.status == 200) {
                        var rsp = JSON.parse(xhr.responseText);
                        if(rsp && rsp.no_file) {
                            return $err.text('No file was added. Please add a file');
                        }
                        if(rsp && rsp.not_audio) {
                            return $err.text('File must be an audio file');
                        }
                        if(rsp && rsp.uploaded) {
                            window.location.reload();
                        }
                    }
                }

                // set form data
                var formData = new FormData();
                formData.append('file', file);
                // open xhr request
                xhr.open('POST', URL, true);
                xhr.send(formData);
            });
        });

        // drag handler
        function dragHandler(e) {
            e.stopPropagation();
            e.preventDefault();
            return (e.type === 'dragover' || e.type === 'dragenter') ?
                    e.target.classList.add('hover') :
                    e.target.classList.remove('hover');
        }

        // select handler
        function fileSelectHandler(e) {
            dragHandler(e);
            // get files from input or drag
            var files = e.target.files || e.dataTransfer.files;
            // set error messages
            if(files.length !== 1) {
                return $err.text('Please choose a single file');
            }
            // set file
            file = files.item(0);
            // make sure file is an image
            if(!audioRegex.test(file.type)) { // test file type
                return $err.text('File type must be mp3, wav or ogg');
            }
            // clear error messages
            $err.html('');
            // if audio, make sure it stops playing
            if($audio) {
                $audio.get(0).pause();
            }
            // show image before upload
            var reader = new FileReader();
            reader.onloadstart = function(e) {
                $$dragZone.innerHTML = [
                    '<p>',
                        'Reading song details...',
                        '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>',
                    '</p>'
                ].join('');
            }
            reader.onerror = function(e) {
                $$dragZone.innerHTML = '<p>Could not display song details... Please try again.</p>';
            }
            reader.onload = function(e) {
                var $container = $('<div class="audio-container">');
                var $name = $('<p>').text(file.name);
                $audio = $('<audio controls>').attr({'src': e.target.result});
                // append audio only when totally finished loading
                var $$audio = $audio.get(0);
                $$audio.oncanplaythrough = function() {
                    // check if extended songPlayer function exists
                    var $songPlayer = $.isFunction($.fn.songPlayer) ?
                                       $audio.songPlayer({hasProgressBar: true}) :
                                       $audio;
                    $container.append($name, $songPlayer);
                    $($$dragZone).html($container);
                    // kill event for not to trigger on progress bar change
                    $$audio.oncanplaythrough = function() {}
                }
                // take care of error loading audio
                $$audio.onerror = function() {
                    $$dragZone.innerHTML = '<p>Could not load audio file. Please try again.</p>';
                }
            }
            // triggers onload event when finishes reading file
            reader.readAsDataURL(file);
        }

    }

});