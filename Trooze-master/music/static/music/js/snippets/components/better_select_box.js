jQuery.fn.extend({

    betterselect: function(optionsArr, options) {
        // if select box has default value, do not override it
        var $select = this;
        if(!$select.val()) {
            var optionText = (options && options.selectPlaceholder) || '--- Choose ---';
            var $placeholderOption = $('<option value=""></option>').text(optionText).css('display', 'none');
            $select.html($placeholderOption);
        }

        // create options div
        var $div = $('<div class="select-div"></div>');
        optionsArr.forEach(function(singleOption) {
            var $opt = $('<div class="select-option-div"></div>');
            $opt.attr('data-value', singleOption.value);
            $opt.text(singleOption.show);
            $div.append($opt);
        });

        // search input to filter options
        var $filter = $('<div>');
        if(!(options && options.noFilter)) {
            $filter = $('<input class="filter-select-options" type="text" value="" class="form-control" />');
            $filter.attr('placeholder', (options && options.filterPlaceholder) || 'Search Options');
            $filter.keyup(function(e) {
                var value = $(this).val();
                $div.children().each(function(idx, opt) {
                    opt.style.display = opt.textContent.toLowerCase().indexOf(value) === -1 ? 'none' : '';
                });
            });
        }

        // insert options div & filter input after select
        var $wrapperDiv = $('<div class="select-options-wrapper"></div>').append($filter, $div).css('display', 'none');
        $select.after($wrapperDiv);
        var nextClickShowOptions = true;
        $select.click(function(e) {
            e.stopPropagation();
            $wrapperDiv.css('display', nextClickShowOptions ? 'block' : 'none');
            if(nextClickShowOptions && !(options && options.noFilter)) {
                $filter.focus();
            }
            if(!nextClickShowOptions) {
                $select.blur();
            }
            nextClickShowOptions = !nextClickShowOptions;
        });
        $wrapperDiv.click(function(e) {
            e.stopPropagation();
        });
        // close select options on document click
        $(document).click(function(e) {
            if(!nextClickShowOptions) {
                $select.click();
            }
        });

        // change select value when option chosen
        var $hiddenOption = $('<option selected="selected"></option>').css('display', 'none');
        $div.click(function(e) {
            var $el = $(e.target);
            $hiddenOption.val($el.data('value')).text($el.text());
            $select.html($hiddenOption);
            $wrapperDiv.css('display', 'none');
            nextClickShowOptions = true;
        });

    }

});