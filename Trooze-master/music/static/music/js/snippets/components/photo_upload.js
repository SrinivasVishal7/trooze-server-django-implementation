jQuery.fn.extend({

    uploadPhoto: function(URL) {

        // globals
        var $filePopup, $$dragZone, $err, file;
        var xhr = new XMLHttpRequest();
        var imageRegex = /^image\/(jpg|png|gif|jpeg)$/i;

        var template_string = `
            <div class="confirm-popup file-upload-popup panel panel-default text-center">
                <div>
                    <div class="panel-heading">
                        <h3>Upload Image</h3>
                    </div>
                    <div class="panel-body">
                        <div class="file-drag">
                            <span>Drag & Drop or Choose from folder</span>
                        </div>
                        <div class="progress photo-upload-progress">
                          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                            Uploading...
                          </div>
                        </div>
                        <div class="text-danger error-message"></div>
                    </div>
                    <input type="file" id="file" accept="image/*" />
                    <div class="buttons">
                        <button class="btn btn-danger" id="upload" type="submit">Upload</button>
                        <button class="btn btn-default cancel">Cancel</button>
                    </div>
                </div>
            </div>
        `;

        // set click listener on upload photo div
        this.on('click', function(e) {
            e.preventDefault();

            // get dom elements
            var $overlay = $('#overlay');
            $filePopup = $(template_string);
            $$dragZone = $filePopup.find('.file-drag').get(0);
            var $progressbar = $filePopup.find('.photo-upload-progress > .progress-bar').width(0);
            $err = $filePopup.find('.error-message');
            var $$input = $filePopup.find('#file').get(0);

            // when drag zone clicked, let choose from folder
            $$dragZone.addEventListener('click', function(e) { $$input.click(); });

            // set listeners to show file
            $$input.addEventListener('change', fileSelectHandler);
            $$dragZone.addEventListener('drop', fileSelectHandler);
            ['dragover', 'dragenter', 'dragleave'].forEach(function(eventType) {
                $$dragZone.addEventListener(eventType, dragHandler);
            });

            // display upload form on the page
            $(document.body).append($filePopup);
            $filePopup.css('display', 'block');
            $overlay.css('display', 'block');

            // kill upload photo popup
            $('.cancel, #overlay').on('click', function() {
                xhr.abort(); // abort upload
                $filePopup.remove();
                $overlay.css('display', 'none');
                $('#overlay').off('click');
                return false;
            });

            // upload image to server
            $('#upload').click(function(e) {
                e.preventDefault();
                if(!file || !file.type) {
                    return $err.text('A file must be supplied');
                }
                if(!imageRegex.test(file.type)) { // test file type
                    return $err.text('File type must be png, jpg, jpeg or gif');
                }
                // show progress bar
                $progressbar.css('display', 'block');
                // xhr on progress
                xhr.upload.onprogress = function(e) {
                    if (e.lengthComputable) {
                        var upload_percentage = Math.round((e.loaded * 100) / e.total);
                        $progressbar.width(upload_percentage + '%'); // show upload progress
                    }
                }
                // xhr on load
                xhr.upload.onload = function(e) {
                    $progressbar.css('display', 'none').width(0);
                }
                xhr.onreadystatechange = function() {
                    if(xhr.readyState == 4 && xhr.status == 200) {
                        var rsp = JSON.parse(xhr.responseText);
                        if(rsp && rsp.no_file) {
                            return $err.text('No file was added. Please add a file');
                        }
                        if(rsp && rsp.not_image) {
                            return $err.text('File must be an image file');
                        }
                        if(rsp && rsp.uploaded) {
                            window.location.reload();
                        }
                    }
                }
                // set form data
                var formData = new FormData();
                formData.append('file', file, file.name);
                // open xhr request
                xhr.open('POST', URL, true);
                xhr.send(formData);
            });
        });

        // drag handler
        function dragHandler(e) {
            e.stopPropagation();
            e.preventDefault();
            return (e.type === 'dragover' || e.type === 'dragenter') ?
                    e.target.classList.add('hover') :
                    e.target.classList.remove('hover');
        }

        // select handler
        function fileSelectHandler(e) {
            dragHandler(e);
            // get files from input or drag
            var files = e.target.files || e.dataTransfer.files;
            // set error messages
            if(files.length !== 1) {
                return $err.text('Please choose a single file');
            }
            // set file
            file = files.item(0);
            // make sure file is an image
            if(!imageRegex.test(file.type)) { // test file type
                return $err.text('File type must be png, jpg, jpeg or gif');
            }
            // clear error messages
            $err.html('');
            // show image before upload
            var reader = new FileReader();
            reader.onloadstart = function(e) {
                $$dragZone.innerHTML = [
                    '<p>',
                        'Preparing image view...',
                        '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>',
                    '</p>'
                ].join('');
            }
            reader.onerror = function(e) {
                $$dragZone.innerHTML = '<p>Could not display image... Please try again.</p>';
            }
            reader.onload = function(e) {
                var $img = $('<img />').attr({
                    'src': e.target.result,
                    'title': file.name
                });
                $($$dragZone).html($img);
            }
            // triggers onload event when finishes reading file
            reader.readAsDataURL(file);
        }

    }

});