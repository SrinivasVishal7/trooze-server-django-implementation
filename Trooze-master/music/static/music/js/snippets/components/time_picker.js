jQuery.fn.extend({

    timepicker: function(options) {

        var $input = this;

        // attach focus listener to main input element
        $input.on('focus', function() {
            // if parent has time picker, do not create another
            var $parent = $input.parent();
            if($parent.find('.timepicker').length !== 0) {
                return;
            }

            // clone template
            var $template = $('.timepicker').clone().css('display', 'block').appendTo($parent.css('position', 'relative'));

            // set header text if provided on init
            if(options && options.headerText) {
                $template.find('#header-text').text(options.headerText);
            }
            // get minutes & hours input field elements
            var $minutes = $template.find('.minute-field');
            var $hour = $template.find('.hour-field');

            // set initial time value to what already there, or current
            var timeText = $input.val();
            if(timeText && timeText.length === 5 && timeText.indexOf(':') !== -1) {
                var parts = timeText.split(':');
                $hour.val(_normalizeHour(parts[0].trim()));
                $minutes.val(_normalizeMinutes(parts[1].trim()));
            }
            else {
                var d = new Date();
                $minutes.val(_normalizeFormat(d.getMinutes()));
                $hour.val(_normalizeFormat(d.getHours()));
            }

            // set click listeners on up and down arrows
            var $hourUp = $template.find('.hour-up');
            var $hourDown = $template.find('.hour-down');
            var $minuteUp = $template.find('.minute-up');
            var $minuteDown = $template.find('.minute-down');
            $hourUp.on('click', function() {
                $hour.val(_changeHour('up', $hour.val()));
            });
            $hourDown.on('click', function() {
                $hour.val(_changeHour('down', $hour.val()));
            });
            $minuteUp.on('click', function() {
                $minutes.val(_changeMinutes('up', $minutes.val()));
            });
            $minuteDown.on('click', function() {
                $minutes.val(_changeMinutes('down', $minutes.val()));
            });

            // do not propagate event to document when template clicked
            $template.on('click', function(e) {
                e.stopPropagation();
            });

            // attach blur listener on document
            $(document).on('click', function(e) {
                // if clicked on input, do not remove template
                if(e.target === $input[0]) {
                    return;
                }
                _setTimeOnInput($input, $hour.val(), $minutes.val());
                $template.remove();
            });
        });

        // set time
        function _setTimeOnInput($domInput, hour, minutes) {
            if(hour && minutes) {
                $domInput.val(_normalizeHour(hour) + ':' + _normalizeMinutes(minutes));
            }
        }

        // normalize time format
        function _normalizeFormat(s) {
            if(typeof(s) !== 'string')
                s = String(s);
            return s.length === 1 ? ('0' + s) : s;
        }

        // normalize hour
        function _normalizeHour(hour) {
            if(typeof(hour) === 'string') {
                hour = Number(hour);
            }
            hour = hour < 0 ? 0 : hour;
            return _normalizeFormat(hour % 24);
        }

        // normalize minutes
        function _normalizeMinutes(minutes) {
            if(typeof(minutes) === 'string') {
                minutes = Number(minutes);
            }
            minutes = minutes < 0 ? 0 : minutes;
            return _normalizeFormat(minutes % 60);
        }

        // change hour up or down
        function _changeHour(upOrDown, initialHour) {
            if(!initialHour) {
                initialHour = 0;
            }
            if(typeof(initialHour) !== 'number') {
                initialHour = Number(initialHour);
            }
            if(initialHour > 23 || initialHour < 0) {
                initialHour = 0;
            }
            if(upOrDown && upOrDown.toLowerCase() === 'up') {
                initialHour = (initialHour + 1) % 24;
            }
            else {
                if(initialHour === 0) {
                    initialHour = 23;
                }
                else {
                    initialHour -= 1;
                }
            }
            return _normalizeFormat(initialHour);
        }

        // change minute up or down
        function _changeMinutes(upOrDown, initialMinute) {
            if(!initialMinute) {
                initialMinute = 0;
            }
            if(typeof(initialMinute) !== 'number') {
                initialMinute = Number(initialMinute);
            }
            if(initialMinute > 59 || initialMinute < 0) {
                initialMinute = 0;
            }
            if(upOrDown && upOrDown.toLowerCase() === 'up') {
                initialMinute = (initialMinute + 1) % 60;
            }
            else { // down
                if(initialMinute === 0) {
                    initialMinute = 59;
                }
                else {
                    initialMinute -= 1;
                }
            }
            return _normalizeFormat(initialMinute);
        }
    }

});