jQuery.fn.extend({

    songPlayer: function(options) {
        // player template
        var template_string = `
            <div class="song-player-wrapper">
                <div class="song-player">
                    <span class="glyphicon glyphicon-minus-sign play-minus float-left"></span>
                    <span class="glyphicon glyphicon-play-circle play-circle"></span>
                    <span class="glyphicon glyphicon-plus-sign play-plus float-right"></span>
                    <div class="clear"></div>
                </div>
                <div class="song-progress-bar" data-toggle="tooltip" data-placement="bottom" title="">
                    <div class="song-progress-bar-warm"></div>
                </div>
            </div>
        `;

        // dom elements
        var $songPlayer = $(template_string);
        var $audio = this.css('display', 'none');
        var $$audio = $audio.get(0);

        var $playCircle = $songPlayer.find('.play-circle');
        var $minus = $songPlayer.find('.play-minus');
        var $plus = $songPlayer.find('.play-plus');
        var $progressBar = $songPlayer.find('.song-progress-bar');
        var $progressWarm = $progressBar.find('.song-progress-bar-warm');

        // control variables
        var isPlaying = false;
        var VOLUME_RATE = 0.2;
        var PROGRESSBAR_WIDTH = 117;
        var DURATION = $$audio.duration;

        // if no progress bar option, hide progress bar
        if(!(options && options.hasProgressBar)) {
            $progressBar.css('display', 'none');
        }
        // if has progress bar, change progress warm location on click
        else {
            $progressBar.click(function(e) {
                var currentTime = getTimeByProgressBarPosition(e);
                $$audio.currentTime = currentTime;
                $progressWarm.width(Math.round(currentTime / DURATION * 100) + '%');
            });

            $progressBar.hover(function(e) {
                var currentTime = getTimeByProgressBarPosition(e);
                var durationRatio = getPlayTimeRatioString(currentTime);
                $progressBar.attr('data-original-title', durationRatio).tooltip('show');
            });

            $progressBar.find('[data-toggle="tooltip"]').tooltip();

            $$audio.onended = function() {
                window.clearInterval(controlsInterval);
                window.clearInterval(progressbarInterval);
                $playCircle.removeClass('glyphicon-pause').addClass('glyphicon-play-circle');
                $$audio.currentTime = 0;
                isPlaying = false;
            }
        }

        // intervals for controls animation & progress bar animation
        var controlsInterval;
        var progressbarInterval;

        // stop propagation
        $songPlayer.click(function(e) { e.stopPropagation(); });

        // play or pause on play circle click
        $playCircle.click(function() {
            if(isPlaying) {
                $$audio.pause();
                $playCircle.removeClass('glyphicon-pause').addClass('glyphicon-play-circle');
                // clear animation on pause
                window.clearInterval(controlsInterval);
                if(options && options.hasProgressBar) {
                    window.clearInterval(progressbarInterval);
                }
            }
            else {
                $$audio.play();
                $playCircle.addClass('glyphicon-pause').removeClass('glyphicon-play-circle');
                // animate controls on play
                var i = 0;
                controlsInterval = window.setInterval(function() {
                    $songPlayer.find('span').css('font-weight', (i++) % 2 === 0 ? 'normal': 'bold');
                }, 800);
                if(options && options.hasProgressBar) {
                    progressbarInterval = window.setInterval(function() {
                        var percentPlayed = Math.round($$audio.currentTime / DURATION * 100) + '%';
                        $progressWarm.width(percentPlayed);
                    }, 1000);
                }
            }
            isPlaying = !isPlaying;
        });

        // control player volume
        $plus.click(function() {
            var volume = $$audio.volume + VOLUME_RATE;
            if(volume <= 1) {
                return $$audio.volume = volume;
            }
            $$audio.volume = 1;
        });

        $minus.click(function() {
            var volume = $$audio.volume - VOLUME_RATE;
            if(volume >= 0) {
                return $$audio.volume = volume;
            }
            $$audio.volume = 0;
        });

        // helper functions
        function getTimeByProgressBarPosition(e) {
            var windowLeftToProgressbarLength = ($(window).width() / 2) + (PROGRESSBAR_WIDTH / 2);
            var progressbarOffsetLeft = $progressBar.offset().left;
            var partPlayedLength = e.clientX - progressbarOffsetLeft;
            var currentTime = Math.round((partPlayedLength / PROGRESSBAR_WIDTH) * DURATION);
            return currentTime;
        }

        function getPlayTimeRatioString(currentTime) {
            // format current time
            var timeMinutes = _addZeroIfSingleDigit(String(Math.max(0, Math.floor(currentTime / 60))));
            var timeSeconds = _addZeroIfSingleDigit(String(Math.max(0, Math.floor(currentTime % 60))));
            var time = timeMinutes + ':' + timeSeconds;
            // format audio duration
            var durationMinutes = _addZeroIfSingleDigit(String(Math.floor(DURATION / 60)));
            var durationSeconds = _addZeroIfSingleDigit(String(Math.floor(DURATION % 60)));
            var duration = durationMinutes + ':' + durationSeconds;
            return (time + ' / ' + duration);
        }

        function _addZeroIfSingleDigit(str) {
            if(str.length === 1) {
                return '0' + str;
            }
            return str;
        }

        // return player
        return $songPlayer;

    }

});