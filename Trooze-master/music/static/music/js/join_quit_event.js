$(document).ready(function() {

$('.join-quit-event').click(function(e) {
    e.preventDefault();
    var $link = $(this);
    var URL = $link.attr('href');
    var $joined = $('.joined-label');
    $.ajax({
        method: 'get',
        url: URL,
        dataType: 'json',
        contentType: 'application/json',
        success: function(result, status, xhr) {
            // if joined
            if(result && result.joined) {
                $link.html('<i class="fa fa-calendar-times-o" aria-hidden="true"></i> Quit');
                $link.attr('href', URL.replace('join', 'quit'))
                     .removeClass('btn-success')
                     .addClass('btn-danger');
                // add join count to view
                if($joined.length > 0) {
                    var joined_count = Number($joined.text().replace(/\D/g, '')) + 1;
                    $joined.text(joined_count + (joined_count == 1 ? ' participant' : ' participants'));
                }
            }
            // if quit
            else {
                $link.html('<i class="fa fa-calendar-check-o" aria-hidden="true"></i> Join');
                $link.attr('href', URL.replace('quit', 'join'))
                     .removeClass('btn-danger')
                     .addClass('btn-success');
                // reduce join count from view
                if($joined.length > 0) {
                    var joined_count = Number($joined.text().replace(/\D/g, '')) - 1;
                    $joined.text(joined_count + (joined_count == 1 ? ' participant' : ' participants'));
                }
            }
        },
        error: function(result, status, xhr) {
            alert('Some error has occurred');
        }
    });
});

});