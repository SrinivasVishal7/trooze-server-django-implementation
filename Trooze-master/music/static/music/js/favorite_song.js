$(document).ready(function() {

$('.favorite-song').click(function(e) {
    e.preventDefault();
    var $link = $(this);
    var URL = $link.attr('href');
    $.ajax({
        method: 'get',
        url: URL,
        dataType: 'json',
        contentType: 'application/json',
        success: function(result, status, xhr) {
            var add_class = result.is_favorite ? 'glyphicon-star favorite' : 'glyphicon-star-empty';
            var remove_class = result.is_favorite ? 'glyphicon-star-empty' : 'glyphicon-star favorite';
            $link.addClass(add_class).removeClass(remove_class);
        },
        error: function(result, status, xhr) {
            alert('Some error has occurred');
        }
    });
});

});