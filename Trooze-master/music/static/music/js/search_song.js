$(document).ready(function() {

    // search song in songs list per album on the album details view
    $('#search-song').keyup(function(e) {
        var query = $(this).val().trim().toLowerCase();
        $('.table > tbody > tr.table-song > td:nth-of-type(2)').each(function() {
            var $td = $(this), $tr = $td.parent();
            if($td.text().toLowerCase().indexOf(query) === -1) {
                return $tr.css('display', 'none');
            }
            $tr.attr('style', '');
        });
    });

});