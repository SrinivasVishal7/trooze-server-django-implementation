$(document).ready(function() {

    // search song in songs lists of all albums on the full songs view
    $('#albums-search-song').keyup(function(e) {
        // sync with get favorites button
        filter_by_favorites = false;
        $('#get-favorites').click();
        // start search
        var query = $(this).val().trim().toLowerCase();
        $('.all-songs-full-table').each(function() {
            var count_hidden_rows = 0;
            var $wrapper = $(this);
            var $trs = $wrapper.find('.table > tbody > tr.table-song');
            var table_rows_number = $trs.length;
            $trs.find('td:nth-of-type(2)').each(function() {
                var $td = $(this);
                if($td.text().toLowerCase().indexOf(query) === -1) {
                    count_hidden_rows++;
                    return $td.parent().css('display', 'none');
                }
                $td.parent().attr('style', '');
            });
            if(count_hidden_rows === table_rows_number) { // all rows hidden, so hide whole table
                return $wrapper.css('display', 'none');
            }
            $wrapper.attr('style', '');
        });
    });


    // filter by favorites or unfilter by favorites
    var filter_by_favorites = true;
    $('#get-favorites').click(function() {
        if(filter_by_favorites) {
            filter_by_favorites = false;
            var $button = $(this);
            $button.find('.get-favorites-text').text('All songs');
            $button.find('.glyphicon').removeClass('glyphicon-thumbs-up').addClass('glyphicon-headphones');
            $('.all-songs-full-table').each(function() {
                var count_hidden_rows = 0;
                var $wrapper = $(this);
                var $trs = $wrapper.find('.table > tbody > tr.table-song');
                var table_rows_number = $trs.length;
                $trs.find('td:nth-of-type(4) > a').each(function() {
                    var $link = $(this);
                    if(!$link.hasClass('favorite')) {
                        count_hidden_rows++;
                        return $link.closest('tr').css('display', 'none');
                    }
                    $link.closest('tr').attr('style', '');
                });
                if(count_hidden_rows === table_rows_number) { // all rows hidden, so hide whole table
                    return $wrapper.css('display', 'none');
                }
                $wrapper.attr('style', '');
            });
        }
        else {
            filter_by_favorites = true;
            var $button = $(this);
            $button.find('.get-favorites-text').text('Favorites');
            $button.find('.glyphicon').removeClass('glyphicon-headphones').addClass('glyphicon-thumbs-up');
            $('.all-songs-full-table').each(function() {
                var $wrapper = $(this);
                var $trs = $wrapper.find('.table > tbody > tr.table-song');
                $trs.each(function() {
                    $(this).attr('style', '');
                });
                $wrapper.attr('style', '');
            });
        }
    });

});