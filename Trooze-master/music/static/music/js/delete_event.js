$(document).ready(function() {

    // delete album/song
    $('.delete-event').on('click', function(e) {
        e.preventDefault();
        var $link = $(this);
        var DELETE_URL = $link.attr('href');

        var $overlay = $('#overlay');
        var $delete_popup = $('#confirm-delete-popup');
        $delete_popup.find('#name').text($link.data('name')); // fill title
        $delete_popup.find('#album-or-song').text('event'); // event
        $delete_popup.css('display', 'block');
        $overlay.css('display', 'block');

        var $form = $delete_popup.find('form');
        $form.attr('action', DELETE_URL); // set url as form action attribute
        $('.cancel-delete, #overlay').on('click', function() {
            $delete_popup.css('display', 'none');
            $overlay.css('display', 'none');
            $('.cancel-delete, #overlay').off('click');
            return false;
        });
    });

});