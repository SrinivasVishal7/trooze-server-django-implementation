$(document).ready(function() {

$('.like-unlike-event').click(function(e) {
    e.preventDefault();
    var $link = $(this);
    var URL = $link.attr('href');
    var $likes = $('.liked-label');
    $.ajax({
        method: 'get',
        url: URL,
        dataType: 'json',
        contentType: 'application/json',
        success: function(result, status, xhr) {
            // if liked
            if(result && result.liked) {
                $link.html('<span class="glyphicon glyphicon-thumbs-down"></span> Unlike');
                $link.attr('href', URL.replace('like', 'unlike'));
                // add like to view
                if($likes.length > 0) {
                    var likes_count = Number($likes.text().replace(/\D/g, '')) + 1;
                    $likes.text(likes_count + (likes_count == 1 ? ' like' : ' likes'));
                }
            }
            // if unliked
            else {
                $link.html('<span class="glyphicon glyphicon-thumbs-up"></span> Like');
                $link.attr('href', URL.replace('unlike', 'like'));
                // reduce like from view
                if($likes.length > 0) {
                    var likes_count = Number($likes.text().replace(/\D/g, '')) - 1;
                    $likes.text(likes_count + (likes_count == 1 ? ' like' : ' likes'));
                }
            }
        },
        error: function(result, status, xhr) {
            alert('Some error has occurred');
        }
    });
});

});