$(document).ready(function() {

    var $playSongPopup = $('#play-song-popup');
    var $playSongText = $('#play-song-text');
    var player = document.getElementById('music-player');
    var close = document.getElementById('play-song-close');

    // play on click
    $('.table-song > td > a.play-song').click(function(e) {
        e.preventDefault();
        player.pause();
        var $link = $(this);
        var AUDIO_SRC = $link.attr('href');
        var SONG_NAME = $link.data('name');
        var SONG_ARTIST = $link.data('artist');
        $playSongText.text(SONG_ARTIST + '- ' + SONG_NAME);
        $playSongPopup.css('display', 'block');
        $(player).attr('src', AUDIO_SRC);
        player.play();

        // when player ends playing
        player.onended = function() {
            $playSongPopup.css('display', 'none');
            $playSongText.text('');
        }
    });

    // close on click
    $(close).click(function() {
        player.pause();
        $playSongPopup.css('display', 'none');
        $playSongText.text('');
    });

});