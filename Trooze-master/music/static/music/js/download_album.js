$(document).ready(function() {

    $('#download-album').click(function(e) {
        e.preventDefault();

        var $overlay = $('#overlay');
        var $downloadPopup = $('#download-album-popup');
        $overlay.css('display', 'block');
        $downloadPopup.css('display', 'block');
        var $yesLabel = $downloadPopup.find('.yes-label');
        $yesLabel.on('click', function() {
            $overlay.css('display', 'none');
            $downloadPopup.css('display', 'none');
            // press all download anchor tags
            var selector = '.table > tbody > tr > td > a.download-song';
            var anchors = document.querySelectorAll(selector);
            var idx = 0;
            var timeout = window.setTimeout(clickLink, 0);
            function clickLink() {
                if(idx === anchors.length)
                    return window.clearTimeout(timeout);
                anchors[idx++].click();
                timeout = window.setTimeout(clickLink, 0);
            }
            $('#overlay, .no-label').off('click');
        });
        $('#overlay, .no-label').on('click', function() {
            $overlay.css('display', 'none');
            $downloadPopup.css('display', 'none');
            $yesLabel.off('click');
            return false;
        });
    });

});