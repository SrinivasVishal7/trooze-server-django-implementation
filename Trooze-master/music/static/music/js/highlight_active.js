$(document).ready(function() {

// highlight 'albums' or 'songs' tag in the navbar
var path = window.location.pathname;
var selector = '.collapse.navbar-collapse > ul.nav:first-child > li';
var child_to_highlight = true;
if(/\/artist\/\d+\/$/g.test(path)) {
    selector += '#artist';
}
else if(path === '/' || path === '/albums/') {
    selector += '#albums';
}
else if(path === '/albums/songs/') {
    selector += '#songs';
}
else {
    child_to_highlight = false;
}
// highlight if should
if(child_to_highlight) {
    $(selector).addClass('active');
}

});