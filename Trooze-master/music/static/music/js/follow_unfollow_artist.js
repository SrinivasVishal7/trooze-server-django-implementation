$(document).ready(function() {

$('.follow-unfollow-artist').click(function(e) {
    e.preventDefault();
    var $link = $(this);
    var $fa = $link.find('.fa');
    var $text = $link.find('.follow-unfollow');
    var URL = $link.attr('href');
    $.ajax({
        method: 'get',
        url: URL,
        dataType: 'json',
        contentType: 'application/json',
        success: function(result, status, xhr) {
            // if started following
            if(result && result.following) {
                $text.text('Unfollow');
                $fa.removeClass('fa-user-plus').addClass('fa-user-times');
                $link.attr('href', URL.replace('follow', 'unfollow'));
                // add follower to view
                var $followers = $('.followers-label');
                if($followers.length > 0) {
                    var followers_count = Number($followers.text().replace(/\D/g, '')) + 1;
                    $followers.text(followers_count + (followers_count == 1 ? ' follower' : ' followers'));
                }
            }
            // if stopped following
            else {
                $text.text('Follow');
                $fa.addClass('fa-user-plus').removeClass('fa-user-times');
                $link.attr('href', URL.replace('unfollow', 'follow'));
                // reduce follower from view
                var $followers = $('.followers-label');
                if($followers.length > 0) {
                    var followers_count = Number($followers.text().replace(/\D/g, '')) - 1;
                    $followers.text(followers_count + (followers_count == 1 ? ' followers' : ' followers'));
                }
            }
        },
        error: function(result, status, xhr) {
            alert('Some error has occurred');
        }
    });
});

});