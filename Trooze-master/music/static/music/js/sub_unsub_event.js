$(document).ready(function() {

$('.sub-unsub-event').click(function(e) {
    e.preventDefault();
    var $link = $(this);
    var URL = $link.attr('href');
    var $subscribers = $('.subscribed-label');
    $.ajax({
        method: 'get',
        url: URL,
        dataType: 'json',
        contentType: 'application/json',
        success: function(result, status, xhr) {
            // if subscribed
            if(result && result.subscribed) {
                $link.html('<i class="fa fa-reply" aria-hidden="true"></i> Unsubscribe');
                $link.attr('href', URL.replace('subscribe', 'unsubscribe'));
                // add subscriber count to view
                if($subscribers.length > 0) {
                    var subscribers_count = Number($subscribers.text().replace(/\D/g, '')) + 1;
                    $subscribers.text(subscribers_count + (subscribers_count == 1 ? ' subscriber' : ' subscribers'));
                }
            }
            // if unsubscribed
            else {
                $link.html('<i class="fa fa-rss" aria-hidden="true"></i> Subscribe');
                $link.attr('href', URL.replace('unsubscribe', 'subscribe'));
                // reduce subscribers count from view
                if($subscribers.length > 0) {
                    var subscribers_count = Number($subscribers.text().replace(/\D/g, '')) - 1;
                    $subscribers.text(subscribers_count + (subscribers_count == 1 ? ' subscriber' : ' subscribers'));
                }
            }
        },
        error: function(result, status, xhr) {
            alert('Some error has occurred');
        }
    });
});

});