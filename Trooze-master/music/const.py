IMAGE_FILE_TYPES = ['jpg', 'jpeg', 'png', 'gif']

AUDIO_FILE_TYPES = ['mp3', 'wav', 'ogg']

MUSICAL_GENRES = [
        ('Art Punk', 'Art Punk'),
        ('Alternative Rock', 'Alternative Rock'),
        ('Blues', 'Blues'),
        ('Classical Music', 'Classical Music'),
        ('Disco', 'Disco'),
        ('Electronic', 'Electronic'),
        ('Folk', 'Folk'),
        ('Grunge', 'Grunge'),
        ('Gospel', 'Gospel'),
        ('Hard Rock', 'Hard Rock'),
        ('Hip Hop', 'Hip Hop'),
        ('House', 'House'),
        ('Indie', 'Indie'),
        ('Instrumental', 'Instrumental'),
        ('Jazz', 'Jazz'),
        ('Latin', 'Latin'),
        ('Metal', 'Metal'),
        ('Musical', 'Musical'),
        ('Opera', 'Opera'),
        ('Pop', 'Pop'),
        ('Soul', 'Soul'),
        ('Techno', 'Techno'),
        ('Trance', 'Trance'),
        ('R&B', 'R&B'),
        ('Reggae', 'Reggae'),
        ('Reggaeton', 'Reggaeton'),
        ('Rap', 'Rap'),
        ('Rock', 'Rock'),
        ('Soundtrack', 'Soundtrack'),
]

COUNTRIES = [
        ('India', 'India')
]

AREAS = [
    ('center', 'Center'),
    ('north', 'North'),
    ('south', 'South'),
    ('west', 'West'),
    ('east', 'East')
]

GENDER = [
    ('male', 'Male'),
    ('female', 'Female'),
    ('other', 'Other')
]

MONEY = [
        ('INR ()', 'India Rupee ()')
]
