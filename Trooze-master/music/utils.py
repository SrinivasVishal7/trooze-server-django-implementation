import random
import string
from django.db.models.query import QuerySet
from django.shortcuts import render
from django.core.files import File
from .models import Album, Song
from .const import IMAGE_FILE_TYPES, AUDIO_FILE_TYPES


def get_context_object(form, album, action):
    context = {
        'form': form,
        'album': album,
        'action': action
    }
    return context


def create_types_for_warning(types):
    last = len(types) - 1
    warn = ''
    for i in range(last + 1):
        if i == last:
            warn += 'or <strong>{0}</strong>'.format(types[i].upper())
        else:
            warn += '<strong>{0}</strong>, '.format(types[i].upper())
    return warn


def bad_image_file_type(request, form, album, action='create'):
    context = get_context_object(form, album, action)
    context['error_message'] = 'Image file must be of type ' + create_types_for_warning(IMAGE_FILE_TYPES)
    return render(request, 'music/album_form.html', context)


def bad_audio_file_type(request, form, album, song, action='create'):
    context = get_context_object(form, album, action)
    context['error_message'] = 'Audio file must be of type ' + create_types_for_warning(AUDIO_FILE_TYPES)
    if song:
        context['song'] = song
    return render(request, 'music/song_form.html', context)


def song_already_exists(request, form, album, song, action='create'):
    context = get_context_object(form, album, action)
    context['error_message'] = 'The song <strong>' + song.name + '</strong> already exists in the album <strong>' + album.name + '</strong>.'
    return render(request, 'music/song_form.html', context)


def album_already_exists(request, form, album, action='create'):
    context = get_context_object(form, album, action)
    context['error_message'] = 'The album <strong>' + album.name + '</strong> already exists.'
    return render(request, 'music/album_form.html', context)


def random_str_generator(size=15, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def create_copied_album(album, user_to_set):
    album_copy = album.get_copy()
    copied_album = Album(**album_copy)  # create instance from object
    if album.logo_file:
        file_name_parts = album.logo_file.name.split('.')
        if len(file_name_parts[0]) > 50:  # don't let too long file names be generated
            file_name_parts[0] = file_name_parts[0][:35]
        copied_file_name = file_name_parts[0] + '_' + random_str_generator() + '.' + file_name_parts[-1]
        copied_album.logo_file = File(album.logo_file, name=copied_file_name)  # generate same file with different name
    copied_album.user = user_to_set  # set current user to own the album
    copied_album.save()
    return copied_album


def create_copied_song(song, album_to_set):
    if not song.is_downloadable or not song.is_playable:
        return
    song_copy = song.get_copy()
    copied_song = Song(**song_copy)  # create instance from object
    file_name_parts = song.audio_file.name.split('.')
    if len(file_name_parts[0]) > 50:  # don't let too long file names be generated
        file_name_parts[0] = file_name_parts[0][:35]
    copied_file_name = file_name_parts[0] + '_' + random_str_generator() + '.' + file_name_parts[-1]
    copied_song.audio_file = File(song.audio_file, name=copied_file_name)  # generate same file with different name
    copied_song.album = album_to_set
    copied_song.save()


def shuffle(arr):
    from random import randint
    # if query set, turn to list
    if isinstance(arr, QuerySet):
        arr = list(arr)
    for i in range(len(arr)):
        for j in range(i):
            shuffle_index = randint(0, i)
            arr[shuffle_index], arr[i] = arr[i], arr[shuffle_index]
    return arr
