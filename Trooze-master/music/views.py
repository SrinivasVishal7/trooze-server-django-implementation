from django.views.generic import (
    View,
    ListView,
)
from django.db.models import (
    Q,
    CharField,
    Value as V,
)
from django.db.models.functions import (
    Length,
    Concat,
)
from django.contrib.auth import (
    authenticate,
    login,
    logout
)
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.forms import modelform_factory
from django.core.paginator import (
    Paginator,
    PageNotAnInteger,
    EmptyPage
)
from django.conf import settings
from .models import (
    Album,
    Song,
    Artist,
    Event,
)
from .forms import (
    UserRegisterForm,
    ArtistForm,
    UserLoginForm,
    AlbumForm,
    SongForm,
    ArtistSongForm,
    EventForm,
)
from .const import (
    IMAGE_FILE_TYPES,
    AUDIO_FILE_TYPES,
)
from .utils import (
    bad_image_file_type,
    bad_audio_file_type,
    song_already_exists,
    album_already_exists,
    create_copied_album,
    create_copied_song
)

"""""""""""""""""""""
    Landing Page
"""""""""""""""""""""


def landing_page(request):
    if request.user.is_authenticated:
        if hasattr(request.user, 'artist'):
            return redirect('music:artist_details', artist_id=request.user.artist.id)
        return redirect('music:albums')
    return render(request, 'music/home/home.html')


"""""""""""""""""""""""
   User Registration
"""""""""""""""""""""""


class UserRegisterView(View):
    form_class = UserRegisterForm
    template_name = 'music/home/registration/register.html'

    def get(self, request):
        context = {'form': self.form_class(), 'not_logged_in': True}
        return render(request, self.template_name, context)

    def post(self, request):
        form = self.form_class(request.POST or None)
        context = {'form': form, 'not_logged_in': True}
        if form.is_valid():
            user = form.save(commit=False)
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user.save()
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('music:albums')
            return render(request, self.template_name, context)
        return render(request, self.template_name, context)


class UserRegisterArtistView(View):
    form_class = UserRegisterForm
    form_class2 = ArtistForm
    template_name = 'music/home/registration/register_artist.html'

    def get(self, request):
        context = {'form': self.form_class(), 'form2': self.form_class2(), 'not_logged_in': True}
        return render(request, self.template_name, context)

    def post(self, request):
        user_form = self.form_class(request.POST or None)
        artist_form = self.form_class2(request.POST or None, request.FILES or None)
        context = {'form': user_form, 'form2': artist_form, 'not_logged_in': True}
        if user_form.is_valid() and artist_form.is_valid():
            user = user_form.save(commit=False)
            user.artist = artist_form.save(commit=False)
            # check uploaded file type
            username = user_form.cleaned_data['username']
            password = user_form.cleaned_data['password1']
            user.save()
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('music:artist_details', artist_id=user.artist.id)
            return render(request, self.template_name, context)
        return render(request, self.template_name, context)


class UserLoginView(View):
    form_class = UserLoginForm
    template_name = 'music/home/registration/login.html'

    def get(self, request):
        return render(request, self.template_name, {'form': self.form_class(), 'not_logged_in': True})

    def post(self, request):
        form = self.form_class()
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                if hasattr(user, 'artist'):
                    return redirect('music:artist_details', artist_id=user.artist.id)
                return redirect('music:albums')
            else:
                return render(request, 'error.html', {'message': 'This account has been disabled'})
        context = {'form': form, 'not_logged_in': True}
        return render(request, self.template_name, context)


def logout_user(request):
    logout(request)
    return redirect('music:login')


"""""""""""""""""
   Album Views
"""""""""""""""""


# albums view
class AlbumsView(LoginRequiredMixin, ListView):
    template_name = 'music/albums.html'
    context_object_name = 'albums'

    def get_queryset(self):
        return Album.get_albums_by_user(self.request.user)


# single album view
@login_required
def album_details(request, album_id):
    album = Album.cached_album_by_pk(album_id, related='user')
    songs = album.song_set.all()
    context = {'songs': songs, 'album': album}
    return render(request, 'music/album_details.html', context)


"""""""""""""""""
  Album Creation
"""""""""""""""""


class AlbumCreateView(LoginRequiredMixin, View):
    form_class = AlbumForm
    template_name = 'music/album_form.html'

    def get(self, request):
        form = self.form_class()
        # if artist, pre put name
        if hasattr(request.user, 'artist'):
            artist_name = request.user.artist.full_name
            form = self.form_class({'artist': artist_name})
        context = {'form': form, 'action': 'create'}
        return render(request, self.template_name, context)

    def post(self, request):
        form = self.form_class(request.POST or None, request.FILES or None)
        if form.is_valid():
            album = form.save(commit=False)
            album.user = request.user
            # check if album already exists
            if Album.get_albums_by_user(request.user).filter(name__exact=album.name).exists():
                return album_already_exists(request, form, album)
            if request.FILES:  # make sure FILES dictionary is not empty
                file_type = album.logo_file.url.split('.')[-1].lower()
                if file_type not in IMAGE_FILE_TYPES:
                    return bad_image_file_type(request, form, album)
            album.save()
            return redirect('music:albums')
        context = {'form': form, 'action': 'create'}
        return render(request, self.template_name, context)


"""""""""""""""""
  Album Update
"""""""""""""""""


class AlbumUpdateView(LoginRequiredMixin, View):
    form_class = modelform_factory(Album, form=AlbumForm, exclude=['logo_file'])
    template_name = 'music/album_form.html'

    def get(self, request, album_id):
        album = Album.cached_album_by_pk(album_id, related='user')
        if album.user != request.user:
            return render(request, 'error.html', {'message': 'You can not edit this album'})
        form = self.form_class(album.as_object())
        context = {'form': form, 'album': album, 'action': 'update'}
        return render(request, self.template_name, context)

    def post(self, request, album_id):
        album = Album.cached_album_by_pk(album_id, related='user')
        if album.user != request.user:
            message = 'You are trying to make an action you are not authorized to do'
            return render(request, 'error.html', {'message': message})
        form = self.form_class(request.POST or None, request.FILES or None)
        if form.is_valid():
            updated_album = form.save(commit=False)
            all_albums_except_current = Album.get_albums_by_user(request.user).exclude(pk=album_id)
            # check if album already exists
            if all_albums_except_current.filter(name__exact=updated_album.name).exists():
                return album_already_exists(request, form, album, action='update')
            album.update_fields(updated_album, exclude=['logo_file', 'user'])
            album.save()
            return redirect('music:album_details', album_id=album_id)
        context = {'form': form, 'album': album, 'action': 'update'}
        return render(request, self.template_name, context)


"""""""""""""""""
  Album Deletion
"""""""""""""""""


# album delete
@login_required
def delete_album(request, album_id):
    album = Album.cached_album_by_pk(album_id, related='user')
    if album.user == request.user:
        album.delete()
        return redirect('music:albums')
    message = 'You can not delete this album'
    return render(request, 'error.html', {'message': message})


"""""""""""""""""""""
  Album Photo Upload
"""""""""""""""""""""


@login_required
@csrf_exempt
def album_photo_upload(request, album_id):
    album = Album.cached_album_by_pk(album_id, related='user')
    if request.user != album.user:
        return render(request, 'error.html', {'message': 'You can not upload a picture for this album'})
    file = request.FILES.get('file')
    if file is None:
        return JsonResponse({'no_file': False})
    file_type = file.name.split('.')[-1].lower()
    if file_type not in IMAGE_FILE_TYPES:
        return JsonResponse({'not_image': True})
    album.file_to_delete = album.logo_file
    album.logo_file = file
    album.save()
    return JsonResponse({'uploaded': True})


"""""""""""""""""
  Album Search
"""""""""""""""""


@login_required
def album_search(request):
    query = request.GET.get('q', None)
    albums = []
    if query:
        albums = Album.get_albums_by_user(request.user)
        albums = albums.filter(
            Q(name__icontains=query) |
            Q(artist__icontains=query)
        ).distinct()
    context = {'albums': albums, 'query': query}
    return render(request, 'music/albums.html', context)


"""""""""""""""""
  Song Creation
"""""""""""""""""


class SongCreateView(LoginRequiredMixin, View):
    form_class = SongForm
    form_class_artist = ArtistSongForm
    template_name = 'music/song_form.html'

    def get(self, request, album_id):
        album = Album.cached_album_by_pk(album_id, related='user')
        if request.user != album.user:
            return render(request, 'error.html', {'message': 'You can not add songs to this album'})
        # create different form to artist or regular user
        context = {'album': album, 'action': 'create'}
        form = self.form_class()
        if hasattr(request.user, 'artist'):
            form = self.form_class_artist()
        context['form'] = form
        return render(request, self.template_name, context)

    def post(self, request, album_id):
        album = Album.cached_album_by_pk(album_id, related='user')
        if album.user != request.user:
            return render(request, 'error.html', {'message': 'User Error'})

        # create different form to artist or regular user
        form = None
        if hasattr(request.user, 'artist'):
            form = self.form_class_artist(request.POST or None, request.FILES or None)
        else:
            form = self.form_class(request.POST or None, request.FILES or None)

        if form.is_valid():
            if album.song_set.count() == settings.MAX_SONGS:
                message = 'A single album may not contain more than {0} songs.'.format(settings.MAX_SONGS)
                return render(request, 'error.html', {'message': message, 'back': True})
            song = form.save(commit=False)
            song.album = album

            if album.song_set.filter(name__exact=song.name).exists():
                return song_already_exists(request, form, album, song)

            if request.FILES:
                file_type = song.audio_file.url.split('.')[-1].lower()
                if file_type not in AUDIO_FILE_TYPES:
                    return bad_audio_file_type(request, form, album, song)
            song.save()
            return redirect('music:album_details', album_id=album_id)

        context = {'form': form, 'album': album, 'action': 'create'}
        return render(request, self.template_name, context)


"""""""""""""""""
  Song Update
"""""""""""""""""


class SongUpdateView(LoginRequiredMixin, View):
    form_class = modelform_factory(Song, SongForm, exclude=['audio_file'])
    form_class_artist = modelform_factory(Song, ArtistSongForm, exclude=['audio_file'])
    template_name = 'music/song_form.html'

    def get(self, request, album_id, song_id):
        album = Album.cached_album_by_pk(album_id, related='user')
        song = Song.cached_song_by_pk(song_id, related='album')
        if (album.user != request.user) or (song.album.user != request.user):
            return render(request, 'error.html', {'message': 'You can not update this song'})
        # create different form to artist or regular user
        form = None
        song_obj = song.as_object()
        if hasattr(request.user, 'artist'):
            form = self.form_class_artist(song_obj)
        else:
            form = self.form_class(song_obj)
        context = {'form': form, 'album': album, 'song': song, 'action': 'update'}
        return render(request, self.template_name, context)

    def post(self, request, album_id, song_id):
        album = Album.cached_album_by_pk(album_id, related='user')
        song = Song.cached_song_by_pk(song_id, related='album')
        if (album.user != request.user) or (song.album.user != request.user):
            return render(request, 'error.html', {'message': 'User Error'})
        # create different form to artist or regular user
        form = None
        if hasattr(request.user, 'artist'):
            form = self.form_class_artist(request.POST or None)
        else:
            form = self.form_class(request.POST or None)
        if form.is_valid():
            updated_song = form.save(commit=False)
            if song.name != updated_song.name:  # if names are different, check if new name exists
                if album.song_set.filter(name__exact=updated_song.name).exists():  # check if song name exists
                    return song_already_exists(request, form, album, updated_song, action='update')
            # update relevant fields to artist or regular user
            if hasattr(request.user, 'artist'):
                song.update_fields(updated_song, exclude=['audio_file', 'album'])
            else:
                song.update_fields(updated_song, fields=['name'])
            song.save()
            return redirect('music:album_details', album_id=album_id)
        context = {'form': form, 'album': album, 'song': song, 'action': 'update'}
        return render(request, self.template_name, context)


"""""""""""""""""
  Song Deletion
"""""""""""""""""


@login_required
def delete_song(request, album_id, song_id):
    album = Album.cached_album_by_pk(album_id, related='user')
    song = Song.cached_song_by_pk(song_id, related='album')
    if (album.user != request.user) or (song.album.user != request.user):
        return render(request, 'error.html', {'message': 'You can not delete this song'})
    song.delete()
    return redirect('music:album_details', album_id=album_id)


"""""""""""""""""
  Song Favorite
"""""""""""""""""


@login_required
def favorite_song(request, album_id, song_id):
    album = Album.cached_album_by_pk(album_id, related='user')
    song = Song.cached_song_by_pk(song_id, related='album')
    if (album.user != request.user) or (song.album.user != request.user):
        return render(request, 'error.html', {'message': 'You have no access to the resource or it does not exist'})
    song.is_favorite = not song.is_favorite
    song.save()
    return JsonResponse({'is_favorite': song.is_favorite})


"""""""""""""""
  All Songs
"""""""""""""""


@login_required
def all_songs(request):
    songs_by_album = {}
    are_songs = False  # variable to check if there are songs to render
    for album in Album.get_albums_by_user(request.user):
        songs = album.song_set.all()
        if len(songs) > 0:
            are_songs = True
        songs_by_album[album] = songs
    context = {'songs_by_album': songs_by_album.items(), 'are_songs': are_songs}
    return render(request, 'music/all_songs.html', context)


"""""""""""""""""
  Upload Song
"""""""""""""""""


@login_required
@csrf_exempt
def upload_song(request, album_id, song_id):
    album = Album.cached_album_by_pk(album_id, related='user')
    song = Song.cached_song_by_pk(song_id, related='album')
    if request.user != album.user or song.album != album:
        return render(request, 'error.html', {'message': 'You can not do this action'})
    file = request.FILES.get('file')
    if file is None:
        JsonResponse({'no_file': False})
    file_type = file.name.split('.')[-1].lower()
    if file_type not in AUDIO_FILE_TYPES:
        return JsonResponse({'not_audio': True})
    song.file_to_delete = song.audio_file
    song.audio_file = file
    song.save()
    return JsonResponse({'uploaded': True})


"""""""""""""""""
  Search Albums
"""""""""""""""""


# render search albums form
@login_required
def search_albums_form(request):
    return render(request, 'music/search_albums_form.html')


# handle search albums
@login_required
def search_albums(request):
    genre = request.GET.get('genre', '')
    artist = request.GET.get('artist', '')
    name = request.GET.get('album', '')
    albums = Album.objects.exclude(user__id=request.user.id).order_by(Length('likers').desc()).distinct()
    if genre:
        albums = albums.filter(genre__icontains=genre).distinct()
    if artist:
        albums = albums.filter(artist__icontains=artist).distinct()
    if name:
        albums = albums.filter(name__icontains=name).distinct()
    paginator = Paginator(albums, settings.ALBUMS_PAGINATE_NUM)  # init paginator
    page = request.GET.get('page')
    try:  # paginate to provided page
        albums = paginator.page(page)
    except PageNotAnInteger:  # paginate to first page
        albums = paginator.page(1)
    except EmptyPage:  # paginate to last page
        albums = paginator.page(paginator.num_pages)
    context = {
        'genre': genre,
        'artist': artist,
        'name': name,
        'albums': albums
    }
    return render(request, 'music/albums.html', context)


"""""""""""""""
  Like Album
"""""""""""""""


@login_required
def like_album(request, album_id):
    album = Album.cached_album_by_pk(album_id, related='user')
    if request.user == album.user:
        return render(request, 'error.html', {'message': 'You can not like your own album'})
    if not album.is_liked_by(request.user):  # check if user already liked this album
        album.likers.add(request.user)  # add user to likers list
    return JsonResponse({'likes': album.likers.count()})


@login_required
def unlike_album(request, album_id):
    album = Album.cached_album_by_pk(album_id, related='user')
    if request.user == album.user:
        return render(request, 'error.html', {'message': 'You can not unlike your own album'})
    if album.is_liked_by(request.user):  # check if user already liked this album
        album.likers.remove(request.user)  # add user to likers list
    return JsonResponse({'likes': album.likers.count()})


"""""""""""""""
  Copy Album
"""""""""""""""


@login_required
def copy_album(request, album_id):
    album = Album.cached_album_by_pk(album_id, related='user')
    if request.user == album.user:
        return render(request, 'error.html', {'message': 'You own this album and so can not copy it'})
    songs = album.song_set.all()
    copied_album = create_copied_album(album, request.user)
    for song in songs:
        create_copied_song(song, copied_album)
    return redirect('music:album_details', album_id=copied_album.id)


"""""""""""""""""
   Artist Views
"""""""""""""""""


@login_required
def artist_details(request, artist_id):
    artist = Artist.cached_artist_by_pk(artist_id, related='user')
    albums = artist.user.album_set.all()
    events = artist.event_set.all()
    context = {'artist': artist, 'albums': albums, 'events': events}
    return render(request, 'music/artist.html', context)


"""""""""""""""""
  Artist Update
"""""""""""""""""


class ArtistUpdateView(LoginRequiredMixin, View):
    form_class = modelform_factory(Artist, ArtistForm, exclude=['picture'])
    template_name = 'music/artist_form.html'

    def get(self, request, artist_id):
        artist = Artist.cached_artist_by_pk(artist_id, related='user')
        if not hasattr(request.user, 'artist'):
            return render(request, 'error.html', {'message': 'This content belongs to an artist'})
        if artist.user != request.user:
            return render(request, 'error.html', {'message': 'You can not update this content'})
        form = self.form_class(artist.as_object())
        context = {'form': form, 'artist': artist}
        return render(request, self.template_name, context)

    def post(self, request, artist_id):
        artist = Artist.cached_artist_by_pk(artist_id, related='user')
        if not hasattr(request.user, 'artist'):
            return render(request, 'error.html', {'message': 'This content belongs to an artist'})
        if artist.user != request.user:
            return render(request, 'error.html', {'message': 'You can not update this content'})
        form = self.form_class(request.POST or None, request.FILES or None)
        if form.is_valid():
            updated_artist = form.save(commit=False)
            artist.update_fields(updated_artist, exclude=['user', 'likers', 'followers', 'picture'])
            artist.save()
            return redirect('music:artist_details', artist_id=artist.id)
        context = {'form': form, 'artist': artist}
        return render(request, self.template_name, context)


"""""""""""""""""""""""""""
  Follow/Unfollow Artist
"""""""""""""""""""""""""""


@login_required
def follow_artist(request, artist_id):
    artist = Artist.cached_artist_by_pk(artist_id, related='user')
    if artist.user == request.user:
        return render(request, 'error.html', {'message': 'You can not follow yourself'})
    if artist.is_followed_by(request.user):
        return render(request, 'error.html', {'message': 'You are already following this artist'})
    artist.followers.add(request.user)
    return JsonResponse({'following': True})


@login_required
def unfollow_artist(request, artist_id):
    artist = Artist.cached_artist_by_pk(artist_id, related='user')
    if artist.user == request.user:
        return render(request, 'error.html', {'message': 'You can not unfollow yourself'})
    if not artist.is_followed_by(request.user):
        return render(request, 'error.html', {'message': 'You are not following this artist anyway'})
    artist.followers.remove(request.user)
    return JsonResponse({'following': False})


"""""""""""""""""""""""
  Like/Unlike Artist
"""""""""""""""""""""""


@login_required
def like_artist(request, artist_id):
    artist = Artist.cached_artist_by_pk(artist_id, related='user')
    if artist.user == request.user:
        return render(request, 'error.html', {'message': 'You can not like yourself'})
    if artist.is_liked_by(request.user):
        return render(request, 'error.html', {'message': 'You have already liked this artist'})
    artist.likers.add(request.user)
    return JsonResponse({'liked': True})


@login_required
def unlike_artist(request, artist_id):
    artist = Artist.cached_artist_by_pk(artist_id, related='user')
    if artist.user == request.user:
        return render(request, 'error.html', {'message': 'You can not unlike yourself'})
    if not artist.is_liked_by(request.user):
        return render(request, 'error.html', {'message': 'You have not liked this artist'})
    artist.likers.remove(request.user)
    return JsonResponse({'liked': False})


"""""""""""""""""""""""
  Artist Photo Upload
"""""""""""""""""""""""


@login_required
@csrf_exempt
def artist_photo_upload(request, artist_id):
    artist = Artist.cached_artist_by_pk(artist_id)
    if not hasattr(request.user, 'artist') or request.user.artist != artist:
        return render(request, 'error.html', {'message': 'You can not upload a picture for this artist'})
    file = request.FILES.get('file')
    if file is None:
        JsonResponse({'no_file': False})
    file_type = file.name.split('.')[-1].lower()
    if file_type not in IMAGE_FILE_TYPES:
        return JsonResponse({'not_image': True})
    artist.file_to_delete = artist.picture
    artist.picture = file
    artist.save()
    return JsonResponse({'uploaded': True})


"""""""""""""""""""""
   Artists Search
"""""""""""""""""""""


@login_required
def search_artists_form(request):
    return render(request, 'music/search_artists_form.html')


@login_required
def search_artists(request):
    location = request.GET.get('location', '')
    genre = request.GET.get('genre', '')
    artist_name = request.GET.get('artist', '')
    album_name = request.GET.get('album', '')
    artists = None
    # if artist, do know show him on results
    if hasattr(request.user, 'artist'):
        artists = Artist.objects.exclude(id=request.user.artist.id)
    else:
        artists = Artist.objects.all()
    if location:
        artists = artists.filter(country__iexact=location)
    if genre:
        artists = artists.filter(genre__iexact=genre)
    if artist_name:
        # populate virtual full_name field
        artists = artists.annotate(full_name=Concat('first_name', V(' '), 'last_name', output_field=CharField()))
        # filter by populated full_name field
        artists = artists.filter(full_name__icontains=artist_name)
    if album_name:
        artists = artists.filter(user__album__name__icontains=album_name)
    print(artists)
    return


"""""""""""""""""
  Create Event
"""""""""""""""""


class ArtistCreateEventView(LoginRequiredMixin, View):
    form_class = EventForm
    template_name = 'music/event_form.html'

    # make sure artist who owns the profile submits the form
    def ensure_artist(self, request, artist_id):
        artist = Artist.cached_artist_by_pk(artist_id, related='user')
        if not hasattr(request.user, 'artist'):
            return render(request, 'error.html', {'message': 'You can not create events'})
        if request.user != artist.user:
            return render(request, 'error.html', {'message': 'You can not create content on this profile'})
        return artist

    def get(self, request, artist_id):
        artist_or_http = self.ensure_artist(request, artist_id)
        # check if artist_or_http is an Artist instance
        if not isinstance(artist_or_http, Artist):
            return artist_or_http
        context = {'form': self.form_class(), 'artist_id': artist_or_http.id, 'action': 'create'}
        return render(request, self.template_name, context)

    def post(self, request, artist_id):
        artist_or_http = self.ensure_artist(request, artist_id)
        # check if artist_or_http is an Artist instance
        if not isinstance(artist_or_http, Artist):
            return artist_or_http
        form = self.form_class(request.POST or None, request.FILES or None)
        if form.is_valid():
            event = form.save(commit=False)
            event.artist = artist_or_http
            event.save()
            return redirect('music:artist_details', artist_id=artist_id)
        context = {'form': form, 'artist_id': artist_id, 'action': 'create'}
        return render(request, self.template_name, context)


"""""""""""""""""
  Event Details
"""""""""""""""""


@login_required
def event_details(request, artist_id, event_id):
    event = Event.cached_event_by_pk(event_id, related='artist')
    context = {'event': event}
    return render(request, 'music/event_details.html', context)


"""""""""""""""""""""
  Event Photo Upload
"""""""""""""""""""""


@login_required
@csrf_exempt
def event_photo_upload(request, artist_id, event_id):
    event = Event.cached_event_by_pk(event_id, related='artist')
    if not hasattr(request.user, 'artist') or request.user.artist != event.artist or event.artist.id != artist_id:
        return render(request, 'error.html', {'message': 'You can not upload a picture for this event'})
    file = request.FILES.get('file')
    if file is None:
        return JsonResponse({'no_file': False})
    file_type = file.name.split('.')[-1].lower()
    if file_type not in IMAGE_FILE_TYPES:
        return JsonResponse({'not_image': True})
    event.file_to_delete = event.place_image
    event.place_image = file
    event.save()
    return JsonResponse({'uploaded': True})


"""""""""""""""""
  Delete Event
"""""""""""""""""


@login_required
def delete_event(request, artist_id, event_id):
    if not hasattr(request.user, 'artist'):
        return render(request, 'error.html', {'message': 'Only an artist can delete event'})
    event = Event.cached_event_by_pk(event_id, related='artist')
    if request.user != event.artist.user:
        return render(request, 'error.html', {'message': 'You can not delete this event'})
    # todo notify subscribers event was deleted it deleted before start time
    event.delete()
    return redirect('music:artist_details', artist_id=event.artist.id)


"""""""""""""""""
  Update Event
"""""""""""""""""


class EventUpdateView(LoginRequiredMixin, View):
    form_class = modelform_factory(Event, EventForm, exclude=['place_image'])
    template_name = 'music/event_form.html'

    # make sure artist who owns the profile submits the form
    def ensure_artist(self, request, artist_id, event_id):
        event = Event.cached_event_by_pk(event_id, related='artist')
        artist = Artist.cached_artist_by_pk(artist_id, related='user')
        if not hasattr(request.user, 'artist'):
            return render(request, 'error.html', {'message': 'You can not update events'})
        # make sure artist is current logged in user, and event belongs to him
        if (request.user != artist.user) or (event.artist != artist):
            return render(request, 'error.html', {'message': 'You can not update content on this profile'})
        return event

    def get(self, request, artist_id, event_id):
        event_or_http = self.ensure_artist(request, artist_id, event_id)
        if not isinstance(event_or_http, Event):
            return event_or_http
        form = self.form_class(event_or_http.as_object())
        context = {'form': form, 'artist_id': artist_id, 'event_id': event_id, 'action': 'update'}
        return render(request, self.template_name, context)

    def post(self, request, artist_id, event_id):
        event_or_http = self.ensure_artist(request, artist_id, event_id)
        if not isinstance(event_or_http, Event):
            return event_or_http
        # Todo notify subscribers asynchronously when event updates (celery integration)


"""""""""""""""""""""
  Like/Unlike Event
"""""""""""""""""""""


@login_required
def like_event(request, artist_id, event_id):
    artist = Artist.cached_artist_by_pk(artist_id, related='user')
    event = Event.cached_event_by_pk(event_id, related='artist')
    if artist != event.artist:
        return render(request, 'error.html', {'message': 'This artist does not have this event'})
    if request.user == artist.user:
        return render(request, 'error.html', {'message': 'You can not like your own event'})
    if event.is_liked_by(request.user):
        return render(request, 'error.html', {'message': 'You already like this event'})
    event.likers.add(request.user)
    return JsonResponse({'liked': True})


@login_required
def unlike_event(request, artist_id, event_id):
    artist = Artist.cached_artist_by_pk(artist_id, related='user')
    event = Event.cached_event_by_pk(event_id, related='artist')
    if artist != event.artist:
        return render(request, 'error.html', {'message': 'This artist does not have this event'})
    if request.user == artist.user:
        return render(request, 'error.html', {'message': 'You can not unlike your own event'})
    if not event.is_liked_by(request.user):
        return render(request, 'error.html', {'message': 'You did not sign like on this event'})
    event.likers.remove(request.user)
    return JsonResponse({'liked': False})


"""""""""""""""""""""""""""""""""""
    Subscribe/Unsubscribe Event
"""""""""""""""""""""""""""""""""""


@login_required
def subscribe_event(request, artist_id, event_id):
    artist = Artist.cached_artist_by_pk(artist_id, related='user')
    event = Event.cached_event_by_pk(event_id, related='artist')
    if artist != event.artist:
        return render(request, 'error.html', {'message': 'This artist does not have this event'})
    if request.user == artist.user:
        return render(request, 'error.html', {'message': 'You can not subscribe to your own event'})
    if event.is_user_subscribed(request.user):
        return render(request, 'error.html', {'message': 'You are already subscribed to event notifications'})
    event.subscribers.add(request.user)
    return JsonResponse({'subscribed': True})


@login_required
def unsubscribe_event(request, artist_id, event_id):
    artist = Artist.cached_artist_by_pk(artist_id, related='user')
    event = Event.cached_event_by_pk(event_id, related='artist')
    if artist != event.artist:
        return render(request, 'error.html', {'message': 'This artist does not have this event'})
    if request.user == artist.user:
        return render(request, 'error.html', {'message': 'You can not unsubscribe from your own event'})
    if not event.is_user_subscribed(request.user):
        return render(request, 'error.html', {'message': 'You were not subscribed to event notifications'})
    event.subscribers.remove(request.user)
    return JsonResponse({'subscribed': False})


"""""""""""""""""""""""""""
    Join/Quit Event
"""""""""""""""""""""""""""


@login_required
def join_event(request, artist_id, event_id):
    artist = Artist.cached_artist_by_pk(artist_id, related='user')
    event = Event.cached_event_by_pk(event_id, related='artist')
    if artist != event.artist:
        return render(request, 'error.html', {'message': 'This artist does not have this event'})
    if request.user == artist.user:
        return render(request, 'error.html', {'message': 'You can not join your own event'})
    if event.is_user_joined(request.user):
        return render(request, 'error.html', {'message': 'You are already participating in this event'})
    event.participants.add(request.user)
    return JsonResponse({'joined': True})


@login_required
def quit_event(request, artist_id, event_id):
    artist = Artist.cached_artist_by_pk(artist_id, related='user')
    event = Event.cached_event_by_pk(event_id, related='artist')
    if artist != event.artist:
        return render(request, 'error.html', {'message': 'This artist does not have this event'})
    if request.user == artist.user:
        return render(request, 'error.html', {'message': 'You can not quit your own event'})
    if not event.is_user_joined(request.user):
        return render(request, 'error.html', {'message': 'You have not joined event from the first place'})
    event.participants.remove(request.user)
    return JsonResponse({'joined': False})

