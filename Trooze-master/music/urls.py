from django.conf.urls import url, include
from . import views

app_name = 'music'

urlpatterns = [
    # albums view
    url(r'^', views.landing_page, name="landing_page"),

    # register user
    url(r'^register/$', views.UserRegisterView.as_view(), name="register"),

    # register artist
    url(r'^register-artist/$', views.UserRegisterArtistView.as_view(), name="register_artist"),

    # login user
    url(r'^login/$', views.UserLoginView.as_view(), name="login"),

    # logout user
    url(r'^logout/$', views.logout_user, name="logout"),

    url(r'^artists/', include([
        # search artists
        url(r'^search/form/$', views.search_artists_form, name="search_artists_form"),
        url(r'^search/$', views.search_artists, name="search_artists"),
    ])),

    url(r'^artist/(?P<artist_id>[0-9]+)/', include([
        # artist details
        url(r'^$', views.artist_details, name="artist_details"),
        # update artist details
        url(r'^update/$', views.ArtistUpdateView.as_view(), name="artist_update"),
        # follow artist
        url(r'^follow/$', views.follow_artist, name="follow_artist"),
        # unfollow artist
        url(r'^unfollow/$', views.unfollow_artist, name="unfollow_artist"),
        # like artist
        url(r'^like/$', views.like_artist, name="like_artist"),
        # unlike artist
        url(r'^unlike/$', views.unlike_artist, name="unlike_artist"),
        # upload photo
        url(r'^upload-photo/$', views.artist_photo_upload, name="artist_photo_upload"),
        # artist events
        url(r'^event/', include([
            # create event
            url(r'^create/$', views.ArtistCreateEventView.as_view(), name="create_event"),
            # specific event
            url(r'^(?P<event_id>[0-9]+)/', include([
                # event details
                url(r'^$', views.event_details, name="event_details"),
                # delete event
                url(r'^delete/$', views.delete_event, name="delete_event"),
                # update event
                url(r'^update/$', views.EventUpdateView.as_view(), name="update_event"),
                # like event
                url(r'^like/$', views.like_event, name="like_event"),
                # unlike event
                url(r'^unlike/$', views.unlike_event, name="unlike_event"),
                # subscribe event
                url(r'^subscribe/$', views.subscribe_event, name="subscribe_event"),
                # unsubscribe event
                url(r'^unsubscribe/$', views.unsubscribe_event, name="unsubscribe_event"),
                # join event
                url(r'^join/$', views.join_event, name="join_event"),
                # quit event
                url(r'^quit/$', views.quit_event, name="quit_event"),
                # upload photo
                url(r'^upload-photo/$', views.event_photo_upload, name="event_photo_upload"),
            ])),
        ])),
    ])),

    url(r'^album/', include([
        # search album
        url(r'^search/$', views.album_search, name="album_search"),
        # create album
        url(r'^create/$', views.AlbumCreateView.as_view(), name="album_create"),
    ])),

    url(r'^album/(?P<album_id>[0-9]+)/', include([
        # single album view
        url(r'^$', views.album_details, name="album_details"),
        # update album
        url(r'^update/$', views.AlbumUpdateView.as_view(), name="album_update"),
        # delete album
        url(r'^delete/$', views.delete_album, name="album_delete"),
        # like album
        url(r'^like/$', views.like_album, name="like_album"),
        # unlike album
        url(r'^unlike/$', views.unlike_album, name="unlike_album"),
        # copy album
        url(r'^copy/$', views.copy_album, name="copy_album"),
        # upload photo
        url(r'^upload-photo/$', views.album_photo_upload, name="album_photo_upload"),
    ])),

    url(r'^albums/', include([
        # albums view
        url(r'^$', views.AlbumsView.as_view(), name="albums"),
        # search for albums
        url(r'^search/form/$', views.search_albums_form, name="search_albums_form"),
        url(r'^search/$', views.search_albums, name="search_albums"),
        # all songs
        url(r'^songs/$', views.all_songs, name="all_songs"),
    ])),


    url(r'^album/(?P<album_id>[0-9]+)/song/', include([
        # create song
        url(r'^create/$', views.SongCreateView.as_view(), name="song_create"),
        # specific song
        url(r'^(?P<song_id>[0-9]+)/', include([
            # update song
            url(r'^update/$', views.SongUpdateView.as_view(), name="song_update"),
            # delete song
            url(r'^delete/$', views.delete_song, name="song_delete"),
            # favorite song
            url(r'^favorite/$', views.favorite_song, name="favorite_song"),
            # upload audio
            url(r'^upload-song/$', views.upload_song, name="upload_song"),
        ])),
    ])),
]
