from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms
from .const import IMAGE_FILE_TYPES
from .models import (
    Artist,
    Event,
    Album,
    Song,
)


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2',)

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError("This email address already exists")
        return email


class UserLoginForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'password',)
        widgets = {
            'password': forms.PasswordInput(attrs={'placeholder': 'Minimum 8 characters'})
        }


class ArtistForm(forms.ModelForm):
    class Meta:
        model = Artist
        fields = (
            'first_name',
            'last_name',
            'stage_name',
            'gender',
            'country',
            'area',
            'about',
            'picture',
            'facebook',
            'instagram',
            'twitter',
        )
        widgets = {
            'gender': forms.RadioSelect(),
            'about': forms.Textarea(attrs={
                'rows': 7,
                'placeholder': 'Write about yourself...'
            }),
            'facebook': forms.TextInput(attrs={'placeholder': 'Your facebook artist page (if exists)'}),
            'instagram': forms.TextInput(attrs={'placeholder': 'Your instagram artist page (if exists)'}),
            'twitter': forms.TextInput(attrs={'placeholder': 'Your twitter artist page (if exists)'})
        }

    def clean_picture(self):
        picture = self.cleaned_data.get('picture')
        file_type = picture.name.split('.')[-1].lower()
        if file_type not in IMAGE_FILE_TYPES:
            err_str = "Picture must be of format "
            for i in range(len(IMAGE_FILE_TYPES)):
                if i == len(IMAGE_FILE_TYPES)-1:
                    err_str += 'or ' + IMAGE_FILE_TYPES[i]
                else:
                    err_str += IMAGE_FILE_TYPES[i] + ', '
            raise forms.ValidationError(err_str)
        return picture


class AlbumForm(forms.ModelForm):
    class Meta:
        model = Album
        fields = ('artist', 'name', 'genre', 'logo', 'logo_file',)
        widgets = {
            'logo': forms.TextInput(attrs={'placeholder': 'Link to an image'})
        }


class SongForm(forms.ModelForm):
    class Meta:
        model = Song
        fields = ('name', 'audio_file',)


class ArtistSongForm(forms.ModelForm):
    class Meta:
        model = Song
        fields = ('name', 'audio_file', 'is_playable', 'is_downloadable',)

    def clean(self):
        cleaned_data = super(ArtistSongForm, self).clean()
        is_playable = cleaned_data.get('is_playable')
        if not is_playable:
            cleaned_data['is_downloadable'] = False
        return cleaned_data


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = (
            'title',
            'start_date',
            'end_date',
            'start_time',
            'end_time',
            'location',
            'country',
            'city',
            'street',
            'street_number',
            'place_image',
            'people_limit',
            'price',
            'currency',
            'description',
        )
        widgets = {
            'location': forms.TextInput(attrs={
                'placeholder': 'Specify country, city, street address & street number'
            }),
            'country': forms.HiddenInput(),
            'city': forms.HiddenInput(),
            'street': forms.HiddenInput(),
            'street_number': forms.HiddenInput(),
            'people_limit': forms.NumberInput(attrs={'placeholder': 'Enter a number'}),
            'description': forms.Textarea(attrs={
                'rows': 12,
                'placeholder': 'Tell people about the event...'
            })
        }

    def clean_place_image(self):
        place_image = self.cleaned_data['place_image']
        if not place_image:
            return place_image
        file_type = place_image.name.split('.')[-1].lower()
        if file_type not in IMAGE_FILE_TYPES:
            err_str = "Picture must be of format "
            for i in range(len(IMAGE_FILE_TYPES)):
                if i == len(IMAGE_FILE_TYPES)-1:
                    err_str += 'or ' + IMAGE_FILE_TYPES[i]
                else:
                    err_str += IMAGE_FILE_TYPES[i] + ', '
            raise forms.ValidationError(err_str)
        return place_image

    def clean(self):
        cleaned_data = super(EventForm, self).clean()

        # check start & end dates
        start_date = cleaned_data.get('start_date')
        end_date = cleaned_data.get('end_date')
        if end_date < start_date:
            raise forms.ValidationError({'end_date': 'End date must be after or same as start date'})

        # check start & end times
        start_time = cleaned_data.get('start_time')
        if len(start_time) != 5 or ':' not in start_time:
            raise forms.ValidationError({'start_date': 'Time format must be (hh):(mm)'})

        end_time = cleaned_data.get('end_time')
        if len(end_time) != 5 or ':' not in end_time:
            raise forms.ValidationError({'end_time': 'Time format must be (hh):(mm)'})

        # check start & end times make sense
        if start_date == end_date:
            start_time_parts = start_time.split(':')
            end_time_parts = end_time.split(':')
            start_hour = start_time_parts[0]
            end_hour = end_time_parts[0]

            if start_hour > end_hour:
                raise forms.ValidationError({'end_time': 'Event end time must be after start time'})

            elif start_hour == end_hour:
                start_minute = start_time_parts[1]
                end_minute = end_time_parts[1]

                if start_minute > end_minute:
                    raise forms.ValidationError({'end_time': 'Event end time must be after start time'})

        # check location fields
        location = cleaned_data.get('location')
        if not location:
            raise forms.ValidationError({'location': 'Location must contain country, city, street and street number'})

        country = cleaned_data.get('country')
        if not country:
            raise forms.ValidationError({'location': 'Country must be mentioned'})

        city = cleaned_data.get('city')
        if not city:
            raise forms.ValidationError({'location': 'City must be mentioned'})

        street = cleaned_data.get('street')
        if not street:
            raise forms.ValidationError({'location': 'street must be mentioned'})

        street_number = cleaned_data.get('street_number')
        if not street_number:
            raise forms.ValidationError({'location': 'Street number must be mentioned'})
